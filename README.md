# neutrinet_cube_install

A script to easily install Neutrinet internetcubes

- [Neutrinet association](https://neutrinet.be)
- [La Brique Internet/Internet Cube project](https://labriqueinter.net/)
- [Yunohost project](https://yunohost.org)

# About this script

This script is meant to make it easier to install a Neutrinet internet cube. Download the neutrinet_cube_install.sh script, run it, and follow the instructions.
```bash
bash neutrinet_cube_install.sh
```

If you are planning on installing multiple cubes, you may consider dedicating a folder on your PC/laptop for this and run
```bash
bash neutrinet_cube_install.sh -p
```
This will create a subfolder `cube_resources` containing the install-sd.sh script and the internetcube images for lime and lime2

For more information you can use the -h option
```bash
bash neutrinet_cube_install.sh -h
```
